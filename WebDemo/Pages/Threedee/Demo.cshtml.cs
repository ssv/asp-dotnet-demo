using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebDemo.Pages.Threedee
{
    public class DemoModel : PageModel
    {
        public string Message { get; set; }

        // That's a response to HTTP GET request.
        public void OnGet()
        {
            Message = "Hello World!";
        }
    }
}
